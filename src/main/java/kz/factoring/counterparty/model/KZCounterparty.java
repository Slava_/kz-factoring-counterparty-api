package kz.factoring.counterparty.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Table(name = "kz_counterparty")
@EqualsAndHashCode(callSuper = true)
public class KZCounterparty extends Counterparty {
    private String bin;
    private String kbe;
    private Set<KZBankDetails> bankDetails = new HashSet<>();

    @OneToMany(orphanRemoval=true, cascade= CascadeType.ALL)
    @JoinColumn(name="kz_counterparty_id")
    public Set<KZBankDetails> getBankDetails() {
        return bankDetails;
    }
}
