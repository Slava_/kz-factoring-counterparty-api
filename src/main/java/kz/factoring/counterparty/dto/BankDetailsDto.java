package kz.factoring.counterparty.dto;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class BankDetailsDto {
    private String iik;
    private String paymentAccount;
    private String corrAccount;
    @NotBlank
    private String bankName;
    @NotBlank
    private String bik;
    @NotBlank
    private String iban;
}
