package kz.factoring.counterparty.validator.constraints;

import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import kz.factoring.counterparty.validator.ValueOf;

public class ValueOfValidator implements ConstraintValidator<ValueOf, String> {

    private Set<String> values;

    @Override
    public void initialize(ValueOf annotation) {
        this.values = Set.of(annotation.values());
    }

    @Override
    public final boolean isValid(String value, ConstraintValidatorContext context) {
        return (value == null) || values.contains(value);
    }
}
