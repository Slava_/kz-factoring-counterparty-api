package kz.factoring.counterparty.service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.CounterpartyNotFoundException;
import kz.factoring.counterparty.model.Counterparty;
import kz.factoring.counterparty.repositiry.CounterpartyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class CounterpartyServiceProvider {

    private final CounterpartyRepository<Counterparty> counterpartyRepository;
    private final Map<String, CounterpartyService<? extends Counterparty>> counterpartyServices;

    public CounterpartyService<? extends Counterparty> getCounterpartyService(Long id) {
        Counterparty counterparty = counterpartyRepository.findById(id)
                .orElseThrow(() -> new CounterpartyNotFoundException(format("Counterparty is not found by id %s", id)));
        String serviceName = CounterpartyMapping.getByType(counterparty.getType()).getServiceName();
        return counterpartyServices.get(serviceName);
    }

    public CounterpartyService<? extends Counterparty> getCounterpartyService(CounterpartyDto counterpartyDto) {
        String serviceName = CounterpartyMapping.getByType(counterpartyDto.getType()).getServiceName();
        return counterpartyServices.get(serviceName);
    }

    public Set<CounterpartyService<? extends Counterparty>> getAllCounterpartyServices() {
        return new HashSet<>(counterpartyServices.values());
    }
}
