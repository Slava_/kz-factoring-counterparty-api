package kz.factoring.counterparty.validator;

import java.util.Set;
import java.util.stream.Stream;
import kz.factoring.counterparty.dto.BankDetailsDto;
import org.springframework.stereotype.Component;

@Component
public class RFBankDetailsValidator extends BankDetailsValidator {

    private static final String RF_TYPE = "RF";

    @Override
    protected boolean isNotValid(Set<BankDetailsDto> bankDetails) {
        return bankDetails.stream()
                .anyMatch(this::checkRFBankDetails);
    }

    @Override
    protected String getCounterpartyType() {
        return RF_TYPE;
    }

    private boolean checkRFBankDetails(BankDetailsDto detail) {
        return Stream.of(detail.getCorrAccount(), detail.getPaymentAccount())
                .anyMatch(String::isEmpty);
    }
}
