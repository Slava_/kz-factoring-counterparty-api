package kz.factoring.counterparty.service;

import kz.factoring.counterparty.model.RFCounterparty;
import org.springframework.stereotype.Service;

@Service("RFCounterpartyService")
public class RFCounterpartyService extends CounterpartyService<RFCounterparty> {
}
