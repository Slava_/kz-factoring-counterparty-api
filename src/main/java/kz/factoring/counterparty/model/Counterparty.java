package kz.factoring.counterparty.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "counterparty")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Counterparty {
    private Long id;
    private String type;
    private String name;
    private String fullName;
    private String webSite;
    private String activity;
    private String email;
    private String fax;
    private Set<Contact> contacts = new HashSet<>();
    private Set<Address> addresses = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    @OneToMany(orphanRemoval=true, cascade= CascadeType.ALL)
    public Set<Contact> getContacts() {
        return contacts;
    }

    @OneToMany(orphanRemoval=true, cascade= CascadeType.ALL)
    public Set<Address> getAddresses() {
        return addresses;
    }
}
