package kz.factoring.counterparty.controller;
;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.service.CounterpartyService;
import kz.factoring.counterparty.service.CounterpartyServiceProvider;
import kz.factoring.counterparty.validator.RequestValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/counterparty")
@RequiredArgsConstructor
public class CounterpartyController {

    private final CounterpartyServiceProvider counterpartyServiceProvider;
    private final List<RequestValidator> requestValidators;

    @GetMapping("/{id}")
    protected CounterpartyDto getCounterparty(@PathVariable Long id) {
        return counterpartyServiceProvider.getCounterpartyService(id).getCounterparty(id);
    }

    @GetMapping
    protected List<CounterpartyDto> getCounterparties() {
        return counterpartyServiceProvider.getAllCounterpartyServices().stream()
                .map(CounterpartyService::getCounterparties)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    protected CounterpartyDto updateCounterparty(@Valid @RequestBody CounterpartyDto counterpartyDto) {
        validateRequest(counterpartyDto);
        return counterpartyServiceProvider.getCounterpartyService(counterpartyDto).updateCounterparty(counterpartyDto);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    protected CounterpartyDto createCounterparty(@Valid @RequestBody CounterpartyDto counterpartyDto) {
        validateRequest(counterpartyDto);
        return counterpartyServiceProvider.getCounterpartyService(counterpartyDto).addCounterparty(counterpartyDto);
    }

    @DeleteMapping
    protected void deleteCounterparty(@Valid @RequestBody CounterpartyDto counterpartyDto) {
        validateRequest(counterpartyDto);
        counterpartyServiceProvider.getCounterpartyService(counterpartyDto).deleteCounterparty(counterpartyDto);
    }

    @DeleteMapping("/{id}")
    public void deleteCounterpartyById(@PathVariable Long id) {
        counterpartyServiceProvider.getCounterpartyService(id).deleteCounterpartyById(id);
    }

    private void validateRequest(CounterpartyDto counterpartyDto) {
        requestValidators.forEach(validator -> validator.validate(counterpartyDto));
    }
}
