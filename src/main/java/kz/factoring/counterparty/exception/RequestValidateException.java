package kz.factoring.counterparty.exception;

public class RequestValidateException extends RuntimeException {
    public RequestValidateException(String message) {
        super(message);
    }
}
