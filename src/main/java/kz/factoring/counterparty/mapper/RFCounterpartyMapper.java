package kz.factoring.counterparty.mapper;

import kz.factoring.counterparty.model.RFCounterparty;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RFCounterpartyMapper extends CounterpartyMapper<RFCounterparty>{
}
