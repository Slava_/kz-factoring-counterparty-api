package kz.factoring.counterparty.validator;

import java.util.stream.Stream;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.RequestValidateException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class RFSpecialFieldsValidator implements RequestValidator{

    private static final String RF_TYPE = "RF";

    @Override
    public void validate(CounterpartyDto request) {
        if (RF_TYPE.equals(request.getType()) && isNotValid(request)) {
            throw new RequestValidateException("Some of these fields are not valid: inn, kpp, ogrn");
        }
    }

    private boolean isNotValid(CounterpartyDto request) {
        return Stream.of(request.getInn(), request.getKpp(), request.getOgrn())
                .anyMatch(StringUtils::isEmpty);
    }
}
