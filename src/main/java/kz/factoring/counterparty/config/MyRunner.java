package kz.factoring.counterparty.config;

import java.util.Set;
import javax.transaction.Transactional;
import kz.factoring.counterparty.model.Address;
import kz.factoring.counterparty.model.Contact;
import kz.factoring.counterparty.model.KZBankDetails;
import kz.factoring.counterparty.model.KZCounterparty;
import kz.factoring.counterparty.model.RFBankDetails;
import kz.factoring.counterparty.model.RFCounterparty;
import kz.factoring.counterparty.repositiry.CounterpartyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(MyRunner.class);

    @Autowired
    private CounterpartyRepository<RFCounterparty> userRepositoryRF;
    @Autowired
    private CounterpartyRepository<KZCounterparty> userRepositoryKZ;

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        logger.info("initializing rf users");

        var rfCounterparty = new RFCounterparty();
        rfCounterparty.setType("RF");
        rfCounterparty.setFullName("ООО \"Агро-Нефте-Прод-Маш\"");
        rfCounterparty.setWebSite("ftgt.ru");
        rfCounterparty.setInn("12423523412");
        rfCounterparty.setKpp("4435342543");
        rfCounterparty.setOgrn("4563453");
        rfCounterparty.setActivity("странная активность");
        rfCounterparty.setEmail("2134@mal.ru");
        rfCounterparty.setFax("483727821830912");
        rfCounterparty.setName("ООО \"Агро-Нефте-Прод-Маш\"");
        var a = new Address();
        a.setFullAddress("г. Москва, Неизвестный переулок д 5");
        a.setType("POSTAL");
        var addresses = Set.of(a);
        rfCounterparty.setAddresses(addresses);
        var cont = new Contact();
        cont.setEmail("2112@mail.ru");
        cont.setFullName("Иванов Иван Иванович");
        cont.setPosition("DIRECTOR");
        cont.setPhoneNumber("3243523432");
        rfCounterparty.setContacts(Set.of(cont));
        var d = new RFBankDetails();
        d.setCorrAccount("24231231412");
        d.setPaymentAccount("3435323423");
        d.setBankName("Сбербанк");
        d.setBik("23523243352");
        d.setIban("2342353454");
        rfCounterparty.setBankDetails(Set.of(d));
        userRepositoryRF.save(rfCounterparty);

        logger.info("initializing users finished {}", rfCounterparty);

        logger.info("initializing kz users");

        var kzCounterparty = new KZCounterparty();
        kzCounterparty.setType("KZ");
        kzCounterparty.setFullName("ООО KZ \"Агро-Нефте-Прод-Маш\"");
        kzCounterparty.setWebSite("KZftgt.ru");
        kzCounterparty.setBin("32523523");
        kzCounterparty.setKbe("34654y5654643");
        kzCounterparty.setActivity("Очень сртанная деятельность");
        kzCounterparty.setEmail("654543@mal.ru");
        kzCounterparty.setFax("483727821830912");
        kzCounterparty.setName("ООО \"Агро-Нефте-Прод-Маш\"");
        var a2 = new Address();
        a2.setFullAddress("г. Казань, Известный переулок д 5");
        a2.setType("POSTAL");
        kzCounterparty.setAddresses(Set.of(a2));
        var cont2 = new Contact();
        cont2.setEmail("2112@mail.ru");
        cont2.setFullName("Иванов Иван Иванович");
        cont2.setPosition("director");
        cont2.setPhoneNumber("3243523432");
        var cont3 = new Contact();
        cont3.setEmail("2112@mail.ru");
        cont3.setFullName("Иванов Иван Иванович");
        cont3.setPosition("general_director");
        cont3.setPhoneNumber("3243523432");
        var cont4 = new Contact();
        cont4.setEmail("2112@mail.ru");
        cont4.setFullName("Иванов Иван Иванович");
        cont4.setPosition("acting_manager");
        cont4.setPhoneNumber("3243523432");
        kzCounterparty.setContacts(Set.of(cont2, cont3, cont4));
        var d2 = new KZBankDetails();
        d2.setBankName("Сбербанк");
        d2.setBik("23523243352");
        d2.setIban("2342353454");
        d2.setIik("439853465873489749832");
        kzCounterparty.setBankDetails(Set.of(d2));
        userRepositoryKZ.save(kzCounterparty);

        logger.info("initializing users finished {}", kzCounterparty);
    }
}
