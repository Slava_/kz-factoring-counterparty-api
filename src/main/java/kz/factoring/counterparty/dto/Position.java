package kz.factoring.counterparty.dto;

import java.util.Arrays;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum Position {

    DIRECTOR("director"),
    GENERAL_DIRECTOR("general_director"),
    ACTING_MANAGER("acting_manager"),
    ACCOUNTANT("accountant"),
    REPRESENTATIVE("representative");

    private final String role;

    public static Position getPositionBy(String role) {
        return Arrays.stream(values())
                .filter(v -> v.role.equals(role))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Unsupported position with role [" + role + "]"));
    }
}
