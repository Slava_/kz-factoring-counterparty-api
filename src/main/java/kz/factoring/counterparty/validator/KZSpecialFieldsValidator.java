package kz.factoring.counterparty.validator;

import java.util.stream.Stream;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.RequestValidateException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class KZSpecialFieldsValidator implements RequestValidator{

    private static final String KZ_TYPE = "KZ";

    @Override
    public void validate(CounterpartyDto request) {
        if (KZ_TYPE.equals(request.getType()) && isNotValid(request)) {
            throw new RequestValidateException("Some of these fields are not valid: bin, kbe");
        }
    }

    private boolean isNotValid(CounterpartyDto request) {
        return Stream.of(request.getBin(), request.getKbe())
                .anyMatch(StringUtils::isEmpty);
    }
}
