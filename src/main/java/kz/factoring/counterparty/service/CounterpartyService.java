package kz.factoring.counterparty.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.CounterpartyNotFoundException;
import kz.factoring.counterparty.mapper.CounterpartyMapper;
import kz.factoring.counterparty.model.Counterparty;
import kz.factoring.counterparty.repositiry.CounterpartyRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.String.format;

@Slf4j
public abstract class CounterpartyService<T extends Counterparty> {

    @Autowired
    private CounterpartyRepository<T> counterpartyRepository;
    @Autowired
    private CounterpartyMapper<T> counterpartyMapper;

    public CounterpartyDto getCounterparty(Long id) {
        Optional<T> counterparty = counterpartyRepository.findById(id);
        return counterparty
                .map(counterpartyMapper::convert)
                .orElseThrow(() -> new CounterpartyNotFoundException(
                        format("Counterparty is not found by id %s", id)));
    }

    public CounterpartyDto addCounterparty(CounterpartyDto counterpartyDto) {
        T counterparty = counterpartyMapper.convert(counterpartyDto);
        return Optional.of(counterpartyRepository.save(counterparty))
                .map(counterpartyMapper::convert)
                .orElseThrow(() -> new RuntimeException(""));
    }

    public CounterpartyDto updateCounterparty(CounterpartyDto counterpartyDto) {
        T newCounterparty = counterpartyMapper.convert(counterpartyDto);
        return Optional.of(counterpartyRepository.save(newCounterparty))
                .map(counterpartyMapper::convert)
                .orElseThrow(() -> new RuntimeException(""));
    }

    public List<CounterpartyDto> getCounterparties() {
        return IterableUtils.toList(counterpartyRepository.findAll()).stream()
                .map(counterparty -> counterpartyMapper.convert(counterparty))
                .collect(Collectors.toList());
    }

    public void deleteCounterparty(CounterpartyDto counterpartyDto) {
        T counterparty = counterpartyMapper.convert(counterpartyDto);
        counterpartyRepository.delete(counterparty);
    }

    public void deleteCounterpartyById(Long id) {
        counterpartyRepository.deleteById(id);
    }
}
