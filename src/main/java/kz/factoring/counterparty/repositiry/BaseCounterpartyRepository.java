package kz.factoring.counterparty.repositiry;

import kz.factoring.counterparty.model.Counterparty;
import org.springframework.stereotype.Repository;

@Repository
public interface BaseCounterpartyRepository extends CounterpartyRepository<Counterparty> {
}
