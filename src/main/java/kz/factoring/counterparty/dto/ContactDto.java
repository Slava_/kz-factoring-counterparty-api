package kz.factoring.counterparty.dto;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class ContactDto {
    @NotBlank
    private String position;
    @NotBlank
    private String fullName;
    @NotBlank
    private String phoneNumber;
    @NotBlank
    private String email;
}
