package kz.factoring.counterparty.validator;

import java.util.Set;
import kz.factoring.counterparty.dto.BankDetailsDto;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.RequestValidateException;

public abstract class BankDetailsValidator implements  RequestValidator {

    @Override
    public void validate(CounterpartyDto request) {
        if (getCounterpartyType().equals(request.getType()) && isNotValid(request.getBankDetails())) {
            throw new RequestValidateException("Bank details are not valid");
        }
    }

    protected abstract boolean isNotValid(Set<BankDetailsDto> bankDetails);

    protected abstract String getCounterpartyType();
}
