package kz.factoring.counterparty.validator;

import java.util.Set;
import kz.factoring.counterparty.dto.AddressDto;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.RequestValidateException;
import kz.factoring.counterparty.properties.CounterpartyProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AddressesValidator implements RequestValidator {

    private final CounterpartyProperties properties;

    @Override
    public void validate(CounterpartyDto request) {
        if (isNotValid(request.getAddresses())) {
            throw new RequestValidateException("Addresses are not valid");
        }
    }

    private boolean isNotValid(Set<AddressDto> addresses) {
        Set<String> allowedTypes = properties.getAddressTypes();
        return addresses.stream()
                .map(AddressDto::getType)
                .anyMatch(type -> !allowedTypes.contains(type));
    }
}
