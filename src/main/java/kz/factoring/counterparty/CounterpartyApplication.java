package kz.factoring.counterparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan(basePackages = "kz.factoring.counterparty.properties")
@SpringBootApplication
public class CounterpartyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CounterpartyApplication.class, args);
	}
}
