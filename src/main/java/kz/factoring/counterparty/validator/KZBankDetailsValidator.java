package kz.factoring.counterparty.validator;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kz.factoring.counterparty.dto.BankDetailsDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class KZBankDetailsValidator extends BankDetailsValidator {

    private static final String KZ_TYPE = "KZ";
    //todo надо описание какие могут быть iik
    private static final Pattern IIK_PATTERN = Pattern.compile("^KZ.+$");

    @Override
    protected boolean isNotValid(Set<BankDetailsDto> bankDetails) {
        return bankDetails.stream()
                .anyMatch(detail -> iikIsNotMatch(detail.getIik()));
    }

    @Override
    protected String getCounterpartyType() {
        return KZ_TYPE;
    }

    private boolean iikIsNotMatch(String iik) {
        if (StringUtils.isEmpty(iik)) {
            return false;
        }
        Matcher matcher = IIK_PATTERN.matcher(iik);
        return !matcher.matches();
    }
}
