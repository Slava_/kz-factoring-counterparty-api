package kz.factoring.counterparty.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@Table(name = "rf_counterparty")
@EqualsAndHashCode(callSuper = true)
public class RFCounterparty extends Counterparty {
    private String inn;
    private String ogrn;
    private String kpp;
    private Set<RFBankDetails> bankDetails = new HashSet<>();

    @OneToMany(orphanRemoval=true, cascade= CascadeType.ALL)
    public Set<RFBankDetails> getBankDetails() {
        return bankDetails;
    }
}