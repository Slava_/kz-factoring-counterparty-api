package kz.factoring.counterparty.properties;

import java.util.HashSet;
import java.util.Set;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("counterparty")
public class CounterpartyProperties {
    private Set<String> availableTypes = new HashSet<>();
    private Set<String> addressTypes = new HashSet<>();
    private Set<String> positions = new HashSet<>();
}
