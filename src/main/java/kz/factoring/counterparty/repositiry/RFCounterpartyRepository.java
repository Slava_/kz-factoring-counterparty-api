package kz.factoring.counterparty.repositiry;

import kz.factoring.counterparty.model.RFCounterparty;
import org.springframework.stereotype.Repository;

@Repository
public interface RFCounterpartyRepository extends CounterpartyRepository<RFCounterparty> {

}
