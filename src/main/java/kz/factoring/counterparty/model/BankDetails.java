package kz.factoring.counterparty.model;

import javax.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
public abstract class BankDetails {
    private String bankName;
    private String bik;
    private String iban;
}
