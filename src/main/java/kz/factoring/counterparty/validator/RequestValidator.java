package kz.factoring.counterparty.validator;

import kz.factoring.counterparty.dto.CounterpartyDto;

public interface RequestValidator {

    void validate(CounterpartyDto request);
}
