package kz.factoring.counterparty.service;

import kz.factoring.counterparty.model.KZCounterparty;
import org.springframework.stereotype.Service;

@Service("KZCounterpartyService")
public class KZCounterpartyService extends CounterpartyService<KZCounterparty> {
}
