package kz.factoring.counterparty.repositiry;

import kz.factoring.counterparty.model.Counterparty;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CounterpartyRepository<T extends Counterparty> extends CrudRepository<T, Long> {
}
