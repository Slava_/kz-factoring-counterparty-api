package kz.factoring.counterparty.exception;

public class CounterpartyNotFoundException extends RuntimeException {

    public CounterpartyNotFoundException(String message) {
        super(message);
    }
}
