package kz.factoring.counterparty.dto;

import javax.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "type")
public class AddressDto {
    @NotBlank
    private String type;
    @NotBlank
    private String fullAddress;
}
