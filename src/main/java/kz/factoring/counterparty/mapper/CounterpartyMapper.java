package kz.factoring.counterparty.mapper;

import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.model.Counterparty;

public interface CounterpartyMapper<T extends Counterparty> {

    CounterpartyDto convert(T source);

    T convert(CounterpartyDto source);
}
