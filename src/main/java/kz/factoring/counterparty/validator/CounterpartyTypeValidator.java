package kz.factoring.counterparty.validator;

import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.RequestValidateException;
import kz.factoring.counterparty.properties.CounterpartyProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CounterpartyTypeValidator implements RequestValidator {

    private final CounterpartyProperties properties;

    @Override
    public void validate(CounterpartyDto request) {
        if (!properties.getAvailableTypes().contains(request.getType())) {
            throw new RequestValidateException("Counterparty type is not valid");
        }
    }
}
