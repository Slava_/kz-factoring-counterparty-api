package kz.factoring.counterparty.mapper;

import kz.factoring.counterparty.model.KZCounterparty;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface KZCounterpartyMapper extends CounterpartyMapper<KZCounterparty>{
}
