package kz.factoring.counterparty.dto;

import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class CounterpartyDto {
    private Long id;
    @NotBlank
    private String type;
    @NotBlank
    private String name;
    @NotBlank
    private String fullName;
    @NotBlank
    private String webSite;
    @NotBlank
    private String activity;
    @NotBlank
    private String email;
    private String fax;
    private String bin;
    private String kbe;
    private String inn;
    private String ogrn;
    private String kpp;
    @NotEmpty
    @Valid
    private Set<ContactDto> contacts;
    @NotEmpty
    @Valid
    private Set<AddressDto> addresses;
    @NotEmpty
    @Valid
    private Set<BankDetailsDto> bankDetails;
}
