package kz.factoring.counterparty.repositiry;

import kz.factoring.counterparty.model.KZCounterparty;
import org.springframework.stereotype.Repository;

@Repository
public interface KZCounterpartyRepository extends CounterpartyRepository<KZCounterparty> {

}
