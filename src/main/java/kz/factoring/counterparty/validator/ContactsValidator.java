package kz.factoring.counterparty.validator;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import kz.factoring.counterparty.dto.ContactDto;
import kz.factoring.counterparty.dto.CounterpartyDto;
import kz.factoring.counterparty.exception.RequestValidateException;
import kz.factoring.counterparty.properties.CounterpartyProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ContactsValidator implements RequestValidator {

    private static final int LIMIT_MANAGING_PERSONNEL = 2;

    private final CounterpartyProperties properties;

    @Override
    public void validate(CounterpartyDto request) {
        if (isNotValid(request.getContacts())) {
            throw new RequestValidateException("Contacts are not valid");
        }
    }

    public boolean isNotValid(Set<ContactDto> contacts) {
        if (!allPositionsAreAllowed(contacts)) {
            return true;
        }
        Map<String, Set<ContactDto>> contactsByPosition = contacts.stream()
                .filter(contact -> !"REPRESENTATIVE".equals(contact.getPosition()))
                .collect(Collectors.groupingBy(ContactDto::getPosition, Collectors.toSet()));

        return !isOnlyOneOfEachPosition(contactsByPosition)
                || numberOfManagingPersonnelIsNotCorrect(contactsByPosition);
    }

    private boolean allPositionsAreAllowed(Set<ContactDto> contacts) {
        Set<String> allowedPositions = properties.getPositions();
        return contacts.stream()
                .allMatch(contact -> allowedPositions.contains(contact.getPosition()));
    }

    private boolean isOnlyOneOfEachPosition(Map<String, Set<ContactDto>> contacts) {
        return contacts.entrySet().stream()
                .noneMatch(entry -> entry.getValue().size() > 1);
    }

    private boolean numberOfManagingPersonnelIsNotCorrect(Map<String, Set<ContactDto>> contacts) {
        long numberOfManagingPersonnel = contacts.entrySet().stream()
                .filter(entry -> !"ACCOUNTANT".equals(entry.getKey()))
                .map(Map.Entry::getValue)
                .count();

        return numberOfManagingPersonnel > LIMIT_MANAGING_PERSONNEL;
    }
}
