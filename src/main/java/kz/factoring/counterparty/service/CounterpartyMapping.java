package kz.factoring.counterparty.service;

import java.util.Arrays;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import static java.lang.String.format;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum CounterpartyMapping {

    KZ("KZ", "KZCounterpartyService"),
    RF("RF", "RFCounterpartyService");

    private final String counterpartyType;
    private final String serviceName;

    public static CounterpartyMapping getByType(String type) {
        return Arrays.stream(values())
                .filter(c -> c.counterpartyType.equals(type))
                .findAny()
                .orElseThrow(() -> new RuntimeException(format("Counterparty type %s is not supported", type)));
    }
}
